package com.zuitt.batch193;

import java.util.Scanner;

public class Activity {
    public static void main (String[] args){
        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();

        System.out.println("Last Name:");
        String lastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade:");
        double sub1 = appScanner.nextDouble();

        System.out.println("Second Subject Grade:");
        double sub2 = appScanner.nextDouble();

        System.out.println("Third Subject Grade:");
        double sub3 = appScanner.nextDouble();

        double totalGrade = sub1 + sub2 + sub3;
        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("your grade average is: " + ((totalGrade) / 3));
    }
}
